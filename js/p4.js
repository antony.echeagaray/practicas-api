function buscarUsuarioPorID(id) {
    const xhr = new XMLHttpRequest();
    const API_URL = `https://jsonplaceholder.typicode.com/users/${id}`;

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
                const HTMLresponse = document.querySelector("#contenedorUsuarios");
                const user = JSON.parse(this.response);
                const tarjetaHTML = crearTarjetaHTML(user);
                HTMLresponse.innerHTML = tarjetaHTML;
            } else {
                alert("Usuario no encontrado o surgío un error al hacer la petición");
            }
        }
    };
    xhr.open("GET", API_URL);
    xhr.send();
}

function crearTarjetaHTML(user) {
    const direccion = `${user.address.street}, ${user.address.suite}, ${user.address.city}, ${user.address.zipcode}`;
    const empresa = user.company ? `${user.company.name}, ${user.company.catchPhrase}, ${user.company.bs}` : 'No disponible';

    return `<div class="tarjeta">
                <h2>${user.name} (${user.username})</h2>
                <p><strong>Email:</strong> ${user.email}</p>
                <p><strong>Dirección:</strong> ${direccion}</p>
                <p><strong>Teléfono:</strong> ${user.phone}</p>
                <p><strong>Sitio web:</strong> <a href="http://${user.website}" target="_blank">${user.website}</a></p>
                <p><strong>Empresa:</strong> ${empresa}</p>
            </div>`;
}

document.getElementById("formularioBusqueda").addEventListener('submit', function(event) {
    event.preventDefault();
    let id = document.getElementById('inputId').value;
    if (id) {
        buscarUsuarioPorID(id);
    } else {
        alert("Por favor, ingrese un ID para buscar.");
    }
});

document.getElementById("btnLimpiar").addEventListener('click', function() {
    let contenedor = document.getElementById('contenedorUsuarios');
    let id = document.getElementById('inputId');
    id.value = "";
    contenedor.innerHTML = "";
});