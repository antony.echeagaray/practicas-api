function cargarDatos() {
    const xhr = new XMLHttpRequest();
    const API_URL = 'https://jsonplaceholder.typicode.com/posts';

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
                const HTMLresponse = document.querySelector("#lista");
                const datos = JSON.parse(this.response);
                const template = datos.map(post =>
                    `<tr><td class="columna1">${post.userId}</td>` +
                    `<td class="columna2">${post.id}</td>` +  
                    `<td class="columna3">${post.title}</td></tr>`).join(""); 
                HTMLresponse.innerHTML = template;
            } else {
                alert("Surgió un error al hacer la petición");
            }
        }
    };
    xhr.open("GET", API_URL);
    xhr.send();
}

document.getElementById("btnCargar").addEventListener('click', cargarDatos);
document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('lista');
    res.innerHTML = "";
});