function cargarUsuarios() {
    const xhr = new XMLHttpRequest();
    const API_URL = 'https://jsonplaceholder.typicode.com/users';

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
                const HTMLresponse = document.querySelector("#contenedorUsuarios");
                const usuarios = JSON.parse(this.response);
                const template = usuarios.map((user, index) => {
                    const delay = index * 100;
                    const ciudad = user.address && user.address.city ? user.address.city : 'No especificado';
                    return `<div class="tarjeta" style="animation-delay: ${delay}ms;" onclick="colocarIDEnFormulario(${user.id})">
                                <h2>ID:${user.id} ${user.name}</h2>
                                <p><strong>Email:</strong> ${user.email}</p>
                                <p><strong>Ciudad:</strong> ${ciudad}</p>
                            </div>`;
                }).join(""); 
                HTMLresponse.innerHTML = template;
            } else {
                alert("Surgió un error al hacer la petición");
            }
        }
    };
    xhr.open("GET", API_URL);
    xhr.send();
}

document.getElementById("btnCargar").addEventListener('click', cargarUsuarios);
document.getElementById("btnLimpiar").addEventListener('click', function() {
    let contenedor = document.getElementById('contenedorUsuarios');
    contenedor.innerHTML = "";
});