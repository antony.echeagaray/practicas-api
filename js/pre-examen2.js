document.getElementById('search-button').addEventListener('click', function() {
    var countryName = document.getElementById('country-name').value;
  
    if (!countryName.trim()) {
      alert('Por favor, ingresa el nombre de un país.');
      return;
    }
  
    fetch(`https://restcountries.com/v3.1/name/${countryName}`)
      .then(response => {
        if (!response.ok) {
          throw new Error('País no encontrado');
        }
        return response.json();
      })
      .then(data => {
        const country = data[0];
        const capital = country.capital ? country.capital[0] : 'No se encontro Capital';
        const languages = country.languages ? Object.values(country.languages).join(', ') : 'No se encontró lenguaje';
        const flagUrl = country.flags.svg || country.flags.png;
  
        document.getElementById('capital').textContent = `Capital: ${capital}`;
        document.getElementById('language').textContent = `Lenguaje: ${languages}`;
        document.getElementById('flag').src = flagUrl; // Asignar la URL de la bandera al src del elemento img
        document.getElementById('flag').style.display = 'block'; // Asegurarse de que la imagen sea visible
      })
      .catch(error => {
        alert(error.message);
        document.getElementById('capital').textContent = 'Capital: ';
        document.getElementById('language').textContent = 'Lenguaje: ';
        document.getElementById('flag').style.display = 'none'; // Ocultar la imagen si hay un error
      });
  });
  
  document.getElementById('clear-button').addEventListener('click', function() {
    document.getElementById('country-name').value = '';
    document.getElementById('capital').textContent = 'Capital:';
    document.getElementById('language').textContent = 'Lenguaje:';
    document.getElementById('flag').style.display = 'none'; // Ocultar la imagen al limpiar
  });
  