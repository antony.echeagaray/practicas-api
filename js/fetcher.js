const llamandoFetch=()=>{
    const url = 'https://jsonplaceholder.typicode.com/todos';
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrarDatos(data))
    .catch((reject=>{
        const lblError = document.getElementById('lblError');
        lblError.innerHTML = "<p>Status: <b>Unexpected Error</b></p>";
    }))
    .finally(() => { 
        const lblOk = document.getElementById('lblError');
        lblOk.innerHTML = "<p>Status: <b>OK</b></p>";
     })
}

const mostrarDatos = (datos) => {
    const contenedor = document.getElementById('respuesta');
    contenedor.innerHTML = '';

    datos.forEach(dato => {
        const elemento = document.createElement('div');
        elemento.innerHTML = `
            <p><b>ID:</b> ${dato.id}</p>
            <p><b>Título: </b>${dato.title}</p>
            <p><b>Completado:</b> ${dato.completed}</p>
        `;
        contenedor.appendChild(elemento);
    });
}

//AWAIT

const llamandoAwait = async () =>{
    try{
        const url = 'https://jsonplaceholder.typicode.com/todos';
        const respuesta = await fetch(url)  
        const data = await respuesta.json()
        mostrarDatos(data);
    }
    catch(error){
        console.log("Surgio un error" + error);
    }
}

const resPositiva = (res) => {
    const lblError = document.getElementById('lblError');
    const lblOk = document.getElementById('lblError');
    if (res === "true") {
        lblError.innerHTML = "<p>Status: <b>Unexpected Error</b></p>";
    } else {
        lblOk.innerHTML = "<p>Status: <b>Ok</b></p>";
    }
};


//AXIOS

const llamandoAxios = async () => {
    const url = 'https://jsonplaceholder.typicode.com/todos';

    axios.get(url)
        .then((res) => {
            mostrarDatos(res.data);
            resPositiva("true"); // Llama a resPositiva con "false" si mostrarDatos se ejecuta con éxito
        })
        .catch((err) => {
            console.log("Surgió un error: " + err);
            resPositiva("false"); // Llama a resPositiva con "true" si hay un error
        });
};

document.getElementById("btnCargar").addEventListener('click', llamandoFetch);
document.getElementById("btnCargarAwait").addEventListener('click', llamandoAwait);
document.getElementById("btnCargarAxios").addEventListener('click', llamandoAxios);
document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('respuesta');
    res.innerHTML = "";
    let label = document.getElementById('lblError');
    label.innerHTML = "<p>Status: <b>Awaiting Response</b></p>";
});