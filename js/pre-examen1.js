document.getElementById('searchButton').addEventListener('click', function() {
    const userId = document.getElementById('userId').value;
    if(!userId.trim()){
        alert('Por favor, ingresa el ID del vendedor');
        return;
      }
    axios.get(`https://jsonplaceholder.typicode.com/users/${userId}`)
    .then(function (response) {
        // Maneja el éxito de la petición aquí
        const user = response.data;
        document.getElementById('name').value = user.name || '';
        document.getElementById('username').value = user.username || '';
        document.getElementById('email').value = user.email || '';
        document.getElementById('street').value = user.address.street || '';
        document.getElementById('suite').value = user.address.suite || '';
        document.getElementById('city').value = user.address.city || '';
    })
    .catch(function (error) {
        // Maneja el error aquí
        console.error('Error al buscar el usuario:', error);
    });
});

document.getElementById("btnLimpiar").addEventListener('click', function() {
    document.getElementById('userId').value ='';
    document.getElementById('name').value ='';
    document.getElementById('username').value ='';
    document.getElementById('email').value = '';
    document.getElementById('street').value = '';
    document.getElementById('suite').value = '';
    document.getElementById('city').value = '';
});
