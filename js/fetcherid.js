const llamandoFetch = (id) => {
    const url = `https://jsonplaceholder.typicode.com/todos/${id}`;
    fetch(url)
    .then(respuesta => respuesta.json())
    .then(data => mostrarDatos(Array.isArray(data) ? data : [data]))
    .catch((error) => {
        const lblError = document.getElementById('lblError');
        lblError.innerHTML = "<p>Status: <b>Unexpected Error</b></p>";
    });
}

const mostrarDatos = (datos) => {
    const contenedor = document.getElementById('respuesta-2');
    contenedor.innerHTML = '';

    datos.forEach(dato => {
        const elemento = document.createElement('div');
        elemento.innerHTML = `
            <p><b>ID:</b> ${dato.id}</p>
            <p><b>Título: </b>${dato.title}</p>
            <p><b>Completado:</b> ${dato.completed}</p>
        `;
        contenedor.appendChild(elemento);
        const lblError = document.getElementById('lblError');
        lblError.innerHTML = "<p>Status: <b>Ok</b></p>";
    });
}

document.getElementById("formularioBusqueda").addEventListener('submit', function(event) {
    event.preventDefault();
    let id = document.getElementById('inputId').value;
    if (id) {
        llamandoFetch(id);
    } else {
        alert("Por favor, ingrese un ID para buscar.");
    }
});

document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('respuesta-2');
    res.innerHTML = "";
    let label = document.getElementById('lblError');
    label.innerHTML = "<p>Status: <b>Awaiting Response</b></p>";
    let input = document.getElementById('inputId');
    input.value ="";
});
