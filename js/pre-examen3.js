//cargar las razas y mandar llamar la funcion para llenar el select box

const cargarRazas=()=>{
    fetch('https://dog.ceo/api/breeds/list')
        .then(response => response.json())
        .then(data => {
            if(data.status === 'success') {
                populateRazas(data.message);
                enableSelectBox();
            } else {
                console.error('Error al obtener las razas de perros');
            }
        })
        .catch(error => console.error('Error:', error));
}

function enableSelectBox() {
    const selectBox = document.getElementById('dog-breeds');
    selectBox.removeAttribute('disabled');
}

//llenar el select box 

function populateRazas(breeds) {
    const selectBox = document.getElementById('dog-breeds');
    selectBox.innerHTML = '';
    
    breeds.forEach(breed => {
        const option = document.createElement('option');
        option.value = breed;
        option.textContent = breed.charAt(0).toUpperCase() + breed.slice(1);
        selectBox.appendChild(option);
    });
}


//Cargar la foto
function mostrarImagen() {
    const selectedBreed = document.getElementById('dog-breeds').value;
    if (!selectedBreed) {
        alert('Por favor, selecciona una raza de perro.');
        return;
    }
    fetch(`https://dog.ceo/api/breed/${selectedBreed}/images/random`)
        .then(response => response.json())
        .then(data => {
            if (data.status === 'success') {
                document.getElementById('dog-image').src = data.message;
            } else {
                console.error('No se pudo obtener la imagen');
            }
        })
        .catch(error => console.error('Error:', error));
}

document.getElementById("btnCargarRazas").addEventListener('click', cargarRazas);
document.getElementById("btnImagenRaza").addEventListener('click', mostrarImagen);