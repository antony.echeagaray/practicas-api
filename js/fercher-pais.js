const llamandoFetch = (countryName) => {
    const formattedCountryName = encodeURIComponent(countryName);
    const url = `https://restcountries.com/v3.1/name/${formattedCountryName}?fullText=true&fields=capital,languages,flags`;
    fetch(url)
    .then(respuesta => {
        if (!respuesta.ok) {
            throw new Error('País no encontrado');
        }
        return respuesta.json();
    })
    .then(data => {
        if (data.length === 0) {
            throw new Error('País no encontrado');
        }
        mostrarDatos(data);
    })
    .catch((error) => {
        const lblError = document.getElementById('lblError');
        lblError.innerHTML = `<p>${error.message}</p>`;
    })
    .finally(() => { 
        const lblOk = document.getElementById('lblError');
        lblOk.innerHTML = "<p>Status: <b>OK</b></p>";
    })
}

const mostrarDatos = (datos) => {
    const contenedor = document.getElementById('respuesta-3');
    contenedor.innerHTML = '';

    datos.forEach(dato => {
        const capital = dato.capital ? dato.capital[0] : 'No disponible';
        const language = dato.languages ? Object.values(dato.languages)[0] : 'No disponible';
        const flag = dato.flags.png; 

        const elemento = document.createElement('div');
        elemento.innerHTML = `
            <p><b>Capital:</b> ${capital}</p>
            <p><b>Idioma:</b> ${language}</p>
            <p><b>Bandera:</b> </p><div><img src="${flag}" alt="Bandera"></div>
        `;
        contenedor.appendChild(elemento);
    });
}

document.getElementById('formularioBusqueda').addEventListener('submit', function(event){
    event.preventDefault();
    const countryName = document.getElementById('nombrePais').value;
    if(countryName){
        llamandoFetch(countryName);
    } else {
        alert("Por favor, ingrese un ID para buscar.");
    }

});

document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('respuesta-2');
    res.innerHTML = `
        <p><b>Capital: </p>
        <p><b>Idioma:</b> </p>
        <p><b>Bandera:</b> </p>
    `;
    let label = document.getElementById('lblError');
    label.innerHTML = "<p>Status: <b>Awaiting Response</b></p>";
    let input = document.getElementById('inputId');
    input.value ="";
});