function cargarDatos(id) {
    const xhr = new XMLHttpRequest();
    const API_URL = `https://jsonplaceholder.typicode.com/posts/${id}`;

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE) {
            if (this.status === 200) {
                const datos = JSON.parse(this.response);
                const HTMLresponse = document.querySelector("#lista");
                const template = `<tr>
                                    <td class="columna1">${datos.userId}</td>
                                    <td class="columna2">${datos.id}</td>  
                                    <td class="columna3">${datos.title}</td>
                                    <td class="columna4">${datos.body}</td>
                                </tr>`; 
                HTMLresponse.innerHTML = template;
            } else {
                alert("Surgió un error al hacer la petición");
            }
        }
    };
    xhr.open("GET", API_URL);
    xhr.send();
}

document.getElementById("btnLimpiar").addEventListener('click', function() {
    let res = document.getElementById('lista');
    res.innerHTML = "";
    let forme = document.getElementById('inputId');
    forme.innerHTML = "";
});

document.getElementById("formularioBusqueda").addEventListener('submit', function(event) {
    event.preventDefault();
    let id = document.getElementById('inputId').value;
    if (id) {
        cargarDatos(id);
    } else {
        alert("Por favor, ingrese un ID para buscar.");
    }
});